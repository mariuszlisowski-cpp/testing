#pragma once

#include <gmock/gmock.h>

#include "legacy_class.hpp"

class LegacyClassMock : public ILegacyClass {                   // derives from interface
public:
    MOCK_METHOD(void, actionOne, (), (override));               // overrides
    MOCK_METHOD(void, actionTwo, (), (override));               // saa
};
