/* g++ test.cpp -std=c++17 -lgtest -lgtest_main -lgmock -pthread */
#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <string>

using string = const std::string&;
using  ::testing::_;
using  ::testing::AtLeast;
using  ::testing::Return;
using  ::testing::Invoke;
using  ::testing::DoDefault;
using  ::testing::DoAll;
using  ::testing::NiceMock;
using  ::testing::StrictMock;

class IDBConnect {
public:
    virtual bool login(string user, string pass) = 0;
    virtual bool logout(string user, string pass) = 0;
};

class DBConnect : public IDBConnect {
public:
    bool login(string user, string pass) override {
        std::cout << "> real login success" << std::endl;
        return true;
    }
    bool logout(string user, string pass) override {
        std::cout << "> real logout success" << std::endl;
        return true;
    }
};

class  DBConnectMock : public IDBConnect {
public:
    MOCK_METHOD(bool, login, (string user, string pass), (override)); 
    MOCK_METHOD(bool, logout, (string user, string pass), (override)); 
};

class DB {
public:
    DB(IDBConnect* db_connect) : db_connect_(db_connect) {}

    bool open_connection(string user, string pass) {
        return db_connect_->login(user, pass);
    } 
    bool close_connection(string user, string pass) {
        return db_connect_->logout(user, pass);
    } 

private:
    IDBConnect* db_connect_;
};

struct DBTestFixture : public ::testing::Test {
    DBConnect db_connect;
    DBConnectMock db_connect_mock;                                          // naggy (default)
    NiceMock<DBConnectMock> db_connect_mock_nice;
    StrictMock<DBConnectMock> db_connect_mock_strict;
};

TEST_F(DBTestFixture, ShouldLoginToMockDataBase) {
    // arrange
    DB data_base(&db_connect_mock);                                         // naggy mock in use
    EXPECT_CALL(db_connect_mock, login(_, _))
        .Times(AtLeast(1))
        .WillOnce(Return(true));                                            // mock login expecting success
    // act
    bool result = data_base.open_connection("root", "toor");
    // assert
    EXPECT_EQ(result, true);
}

TEST_F(DBTestFixture, ShouldLoginToMockDataBaseStrict) {
    /* arrange */
    DB data_base(&db_connect_mock_strict);                                  // strict mock in use
    EXPECT_CALL(db_connect_mock_strict, login(_, _))
        .Times(AtLeast(1))
        .WillOnce(Return(true));  // mock login expecting success
    /* act */
    bool result = data_base.open_connection("root", "toor");
    /* assert */
    EXPECT_EQ(result, true);
}

TEST_F(DBTestFixture, ShouldLoginToRealDataBase) {
    /* arrange */
    DB data_base(&db_connect_mock);
    EXPECT_CALL(db_connect_mock, login(_, _))
        .Times(AtLeast(1))
        .WillOnce(Invoke(&db_connect, &DBConnect::login));                  // invoke real login
    /* act */
    bool result = data_base.open_connection("root", "toor");
    /* assert */
    EXPECT_EQ(result, true);
}

struct Invoked {                                                            // called as default
    bool invoke_on_call_bool() {
        std::cout << "> invoked on call, returns bool" << std::endl;
        return true;
    }
    void invoke_on_call_void() {
        std::cout << "> invoked on call, returns void" << std::endl;
    }
};

TEST_F(DBTestFixture, ShouldLoginToMockDataBaseInvoke) {
    /* arrange */
    DB data_base(&db_connect_mock_strict);
    Invoked invoked;
    ON_CALL(db_connect_mock_strict, login(_, _))
        .WillByDefault(Invoke(&invoked, &Invoked::invoke_on_call_bool));    // expects bool
    EXPECT_CALL(db_connect_mock_strict, login(_, _))
        .Times(AtLeast(1))
        .WillOnce(DoDefault());                                             // call default
    /* act */
    bool result = data_base.open_connection("root", "toor");
    /* assert */
    EXPECT_EQ(result, true);
}

TEST_F(DBTestFixture, ShouldLoginToMockDataBaseInvokeMany) {
    /* arrange */
    DB data_base(&db_connect_mock_strict);
    Invoked invoked;
    EXPECT_CALL(db_connect_mock_strict, login(_, _))
        .Times(AtLeast(1))
        .WillOnce(DoAll(Invoke(&invoked, &Invoked::invoke_on_call_void),    // expects void
                        Return(true)));                                     // expects bool at the end
    /* act */
    bool result = data_base.open_connection("root", "toor");
    /* assert */
    EXPECT_EQ(result, true);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
