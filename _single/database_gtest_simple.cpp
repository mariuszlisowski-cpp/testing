/* g++ mock.cpp -std=c++17 -lgtest -lgmock -pthread */
#include <gmock/gmock.h>

using string = const std::string&;
using  ::testing::_;
using  ::testing::Return;
using  ::testing::AtLeast;

class IDBConnect {                                                          // interface
public:
    virtual bool login(string user, string pass) = 0;
    virtual bool logout(string user) = 0;
    virtual int fetch(size_t index) = 0;
};

class DBConnect : public IDBConnect {                                       // empty class
};                                                                          // CANNOT be tested yet

class  DBConnectMock : public IDBConnect {                                  // thus is mocked here
public:                                                                     // implementing an interface
    MOCK_METHOD(bool, login, (string user, string pass), (override));
    MOCK_METHOD(bool, logout, (string user), (override));
    MOCK_METHOD(int, fetch, (size_t index), (override));
};

class DB {                                                                  // class using interface
public:
    DB(IDBConnect& db_connect) : db_connect_(db_connect) {}                 // dependency injection (via ref)

    bool open_connection(string user, string pass) {                        // tested method
        if (!db_connect_.login(user, pass)) {                               // FIRST call
            // e.g. try to re-authorize                                      
        }                                                                   // mock will return true/false
        return db_connect_.login(user, pass);                               // SECOND call...
    }                                                                       // see 'AuthorizedLogin' test
    bool close_connection(string user) {
        return db_connect_.logout(user);                                    // ONE call only...
    }                                                                       // see 'SuccessfulLogout' test
    int fetch_record(size_t index) {
        return db_connect_.fetch(index);
    }

private:
    IDBConnect& db_connect_;
};

struct DBTest : public ::testing::Test {                                    // common fixture for later tests
    DBConnectMock db_connect_mock;
    DB sut{db_connect_mock};                                                // sut mocked
};

TEST_F(DBTest, AuthorizedLogin) {
    /* arrange */
    EXPECT_CALL(db_connect_mock, login(_, _))                               // expect calling mock with arguments
        .Times(2)                                                           // as placeholders (one call expected)
        .WillRepeatedly(Return(true));                                      // will return TRUE many times...
    /* act */;                                                              // so login can be re-validated
    auto result = sut.open_connection("root", "toor");                      // one call HERE but second...
    /* assert */                                                            // in DB implementation
    EXPECT_TRUE(result);
}

TEST_F(DBTest, UnautorizedLogin) {
    /* arrange */
    ON_CALL(db_connect_mock, login(_, _))                                   // uninteresting mock function call
        .WillByDefault(Return(false));                                      // when login called return false
    EXPECT_CALL(db_connect_mock, login(_, _))                               // check FIRST (and saturated)
        .WillRepeatedly(Return(false));                                     // and will return false
    /* act */;
    auto result = sut.open_connection("unautorized", "incorrect");          // login is called HERE
    /* assert */                                                            // mocked method will be called
    EXPECT_FALSE(result);
}

TEST_F(DBTest, SuccessfulLogout) {
    /* arrange */
    EXPECT_CALL(db_connect_mock, logout(_))
        .Times(AtLeast(1))                                                  // at least ONE call expected
        .WillOnce(Return(true));                                            // ONCE will return true...
    /* act */;                                                              // as is always correct (no password)
    auto result = sut.close_connection("root");                             // ONE call of mocked logout
    /* assert */                                                            // mocked method will be called
    EXPECT_TRUE(result);
}

TEST(DBTestAlone, LoginCall) {
    /* arrange */
    ::testing::NiceMock<DBConnectMock> db_connect_mock_nice;                // nice mock thus...
    DB sut{db_connect_mock_nice};                                           // no warnings about...
    ON_CALL(db_connect_mock_nice, login(_, _))                              // uninteresting mock function call
        .WillByDefault(Return(false));                                      // when login called always return false
    /* act */;
    auto result = sut.open_connection("unautorized", "incorrect");          // login is called HERE
    /* assert */                                                            // mocked method will be called
    EXPECT_FALSE(result);
}

TEST(DBTestAlone, NoLoginCall) {
    /* arrange */
    ::testing::StrictMock<DBConnectMock> db_connect_mock_strict;            // strict but...
    DB sut{db_connect_mock_strict};                                         // no warnings about...
    ON_CALL(db_connect_mock_strict, login(_, _))                            // uninteresting mock function call
        .WillByDefault(Return(false));                                      // when login called always return false
    /* act - no action */;
    /* assert - obvious assertion */
    EXPECT_FALSE(false);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
