#pragma once

class LegacyClass {                                             // unchanged class
public:
    virtual ~LegacyClass() = default;

    virtual void actionOne();
    virtual void actionTwo();
};

inline void LegacyClass::actionOne() {};
inline void LegacyClass::actionTwo() {};
