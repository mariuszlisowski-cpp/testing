#pragma once

#include "days.hpp"

class ICalendar {
public:
   virtual ~ICalendar() = default;
   virtual Days get_weekday() = 0;
};
