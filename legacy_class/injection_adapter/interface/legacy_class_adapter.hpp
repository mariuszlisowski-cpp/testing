#pragma once

#include "../legacy_class.hpp"
#include "ilegacy_class.hpp"

class LegacyClassAdapter : public ILegacyClass {
public:
    LegacyClassAdapter() {
        legacy_class_ = new LegacyClass();
    }
    ~LegacyClassAdapter() {
        delete legacy_class_;
    }

    void actionOne() override;
    void actionTwo() override;

private:
    LegacyClass* legacy_class_;    
};

/* proxy method */
inline void LegacyClassAdapter::actionOne() {
    legacy_class_->actionOne();
}

/* proxy method */
inline void LegacyClassAdapter::actionTwo() {
    legacy_class_->actionTwo();
}

