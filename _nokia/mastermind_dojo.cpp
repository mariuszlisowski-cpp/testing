#include <iostream>
#include <sstream>

struct TestCase { std::string name{}; std::ostringstream failures{}; };
template <typename T> void operator | (TestCase& testCase, T&& testBody) {
    testBody(); auto failures = testCase.failures.str();
    if (failures.empty()) std::cout << testCase.name << ": SUCCESS\n";
    else std::cerr << testCase.name << ": FAILURE\n" << failures;
}
void addFailure(std::ostream& os, const char* file, unsigned line, const char* condition) {
    os << file << ":" << line << ": Failed: " << condition << "\n";
}
#define TEST(name) TestCase name{#name}; name | [&, &failures = name.failures]
#define EXPECT(cond) if (cond) {} else addFailure(failures, __FILE__, __LINE__, #cond)
#define ASSERT(cond) if (cond) {} else return addFailure(failures, __FILE__, __LINE__, #cond)

#include <vector>
#include <functional>
#include <algorithm>

class Mastermind 
{
public: 

Mastermind(std::function<int()> number_generator) 
    :   number_generator(number_generator) {}

    auto evaluate(std::vector<int> inputData)
    {   
        std::pair<int,int> expectedSolution{0,0};

        for(int i=0; i<inputData.size(); i++) {
            if(inputData[i] == solutions[i]) {
                expectedSolution.first++;
            } else {
                for(int j=0; j<solutions.size(); j++) {
                    if(inputData[i] == solutions[j]) {
                        expectedSolution.second++;
                    }
                }
            }
        }
        return expectedSolution;
    }   

    void generate_solution(int num_of_solutions)
    {
        solutions.resize(num_of_solutions);
       std::generate(solutions.begin(), solutions.end(),number_generator);
    }

    auto get_solution()
    {
        return solutions;
    }

    int get_solutions_size()
    {
        return solutions.size();
    }

private:
    int solution;
    std::vector<int> solutions;
    std::function<int()> number_generator;
};

int number_generator();

int main()
{
    TEST(shouldGenerateSolution) {
        Mastermind sut(number_generator);
        std::vector<int> test {1,2,3};
        sut.generate_solution(3);
        EXPECT(sut.get_solution() == test);
    };

     TEST(chechIfSolutionSizeEqualsInputSize)
    {    
        Mastermind sut(number_generator);
        sut.generate_solution(3);
        EXPECT(sut.get_solutions_size()==3);
    };

    TEST(ComparingGeneratedVectorWithExpectedHardcodecVec) {
        int i = 2;
        auto func = [&i] () {return ++i;};
        Mastermind sut(func);

        std::vector<int> test {3,4,5};
        sut.generate_solution(3);
        EXPECT(sut.get_solution() == test);
    };

     TEST(shouldCompareInputDataWithSulotionData) {
        int i = 2;
        auto func = [&i] () {return ++i;};
        Mastermind sut(func);
        sut.generate_solution(3);
        std::vector<int> inputdata{1,2,3};
        std::pair<int,int> expectedSolution {0,1};

        EXPECT(sut.evaluate(inputdata) == expectedSolution);
     };

}

int number_generator()
{
    static int i=0;
    return ++i;
}
