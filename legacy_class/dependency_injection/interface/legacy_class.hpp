#pragma once

#include "ilegacy_class.hpp"

class LegacyClass : public ILegacyClass {                           // class CAN be changed
public:                                                             // so can inherit from interface
    virtual ~LegacyClass() = default;

    virtual void actionOne() override;                              // overrides
    virtual void actionTwo() override;                              // saa
};

inline void LegacyClass::actionOne() {};
inline void LegacyClass::actionTwo() {};
