#include <gtest/gtest.h>

#include "factorial_tgamma.hpp"

TEST(FactorialTGammaTest, HandlesZeroInput) {
  EXPECT_EQ(factorial_tgamma(0), 1);
}

TEST(FactorialTGammaTest, HandlesPositiveInput) {
  EXPECT_EQ(factorial_tgamma(1), 1);
  EXPECT_EQ(factorial_tgamma(2), 2);
  EXPECT_EQ(factorial_tgamma(3), 6);
  EXPECT_EQ(factorial_tgamma(8), 40320);
}
