# pragma once

#include <iterator>
#include <vector>

#include <iostream>

template<typename T>
auto findInVector(const std::vector<T>& v, T value) {
    auto it = std::find(begin(v), end(v), value);
    if (it != v.end()) {
        return std::distance(v.begin(), it);                // or it - v.begin();
    }
    return NULL;
}
