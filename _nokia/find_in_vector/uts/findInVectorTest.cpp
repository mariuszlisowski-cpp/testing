#include <gtest/gtest.h>
#include <vector>

#include "findInVector.hpp"

struct FindInVectorTest : public ::testing::Test {
    std::vector<int> v{ 1, 2, 3, 4, 5, 1 };
};

TEST_F(FindInVectorTest, ShouldReturnNullptr) {
    EXPECT_EQ(findInVector(v, 9), NULL);
}

TEST_F(FindInVectorTest, ShouldReturnProperPosition) {
    EXPECT_EQ(findInVector(v, 5), 4);
}
