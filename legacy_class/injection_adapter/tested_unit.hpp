#pragma once

#include "legacy_class_adapter.hpp"
#include "ilegacy_class.hpp"

class TestedUnit {
public:
    /* dependency injection */
    TestedUnit() = default;                                         // when called initializes private memmber
    explicit TestedUnit(ILegacyClass* legacy_class);                // injection of other implementation...
    ~TestedUnit();                                                  // of LegacyClass (e.g. mocked)

    void invokeActionOne();

private:
    ILegacyClass* legacy_class_ = new LegacyClassAdapter();         // default c'tor creates fully functional
};                                                                  // actual LegacyClass with all implementations

inline TestedUnit::TestedUnit(ILegacyClass* legacy_class)
    : legacy_class_(legacy_class) {}

inline TestedUnit::~TestedUnit() {
    delete legacy_class_;
}

inline void TestedUnit::invokeActionOne() {
    legacy_class_->actionOne();
}
