# pragma once

#include <string>

std::string spacesToUnderscoresRemoveNumbers(const std::string& str);
bool sameString(const std::string& lhs, const std::string& rhs);
const char* errorString();
