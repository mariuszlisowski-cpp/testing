cmake_minimum_required(VERSION 3.14)
project(factorial)

set(CMAKE_CXX_STANDARD 17)

# local gtest
find_package(GTest REQUIRED)

# add a library to be tested
add_library(factorial factorial.cpp)   

# add test suite
add_executable(factorial-ut factorial-ut.cpp)

# link test suite, tested library and gtest entry point
target_link_libraries(factorial-ut factorial GTest::gtest_main)

# configure ctest (ctest -C Debug)
enable_testing()
add_test(NAME factorial_test COMMAND factorial-ut)
