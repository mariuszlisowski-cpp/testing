#include "calendar.hpp"

#include <ctime>

/* to be mocked */
Days Calendar::get_weekday() {                                      // can only return current day
    std::tm time_in;                                                // thus unable to be used in tests
    std::time_t time_temp = std::mktime(&time_in);
    const std::tm* time_out = std::localtime(&time_temp);

    return static_cast<Days>(time_out->tm_wday);
}
