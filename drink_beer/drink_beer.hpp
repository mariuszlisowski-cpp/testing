#pragma once

#include "calendar.hpp"
#include "days.hpp"

struct DrinkBeer {
    DrinkBeer(ICalendar* calendar);
    bool can_drink_beer_today();

    ICalendar* calendar_;
};
