#include <gtest/gtest.h>
#include <string>

/* Please prepare a function which take a single string as and input and check if square braces are balanced.
   So it means that we have the same number of "[" and "]" Please assume that string include only braces.
   There is no need to prepare handling for othe chars.
        "[[]]" valid
        "[[[]" invalid
        "[]][" should be considered as invalid
        "[][]" should be considered as valid
    Please use TDD approach in your implementation.
 */
bool are_balanced_brackets(const std::string& sentence) {
    return true;
};

TEST(BalanceBracketTest, ShouldBeValid) {
    // arrange

    // act

    // assert
    EXPECT_TRUE(are_balanced_brackets("[[]]"));
}

TEST(BalanceBracketTest, ShouldBeInvalid) {
    // arrange

    // act

    // assert
    EXPECT_FALSE(are_balanced_brackets("[[[]"));
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
