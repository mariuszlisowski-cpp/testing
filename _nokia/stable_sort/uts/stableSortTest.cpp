#include <gtest/gtest.h>

#include "stableSort.hpp"

#include <algorithm>
#include <vector>

struct Initialize : public ::testing::Test {
    std::vector<std::pair<int, int>> pairs{
        {2, 8},
        {1, 9},
        {2, 6},
        {3, 5},
        {2, 7},
    };
};

TEST_F(Initialize, ShouldDoSth) {
    std::stable_sort(begin(pairs), end(pairs));

}
