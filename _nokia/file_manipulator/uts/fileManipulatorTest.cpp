#include "gtest/internal/gtest-string.h"
#include <gtest/gtest.h>

#include <algorithm>
#include <fstream>
#include <ios>
#include <iterator>
#include <string>
#include <vector>

#include "fileManipulator.hpp"

struct FileManipulatorTest : public ::testing::Test {
    FileManipulator fileManipulator;
    std::vector<std::string> words;
    static const std::string filename;

    void SetUp() override {
        std::fstream file(filename, std::ios::in);
        std::copy(std::istream_iterator<std::string>(file), {},
                  std::back_inserter(words));
    }

};
const std::string FileManipulatorTest::filename = "fileManipulatorTestFile.txt";

TEST_F(FileManipulatorTest, ShouldDoSth) {

}
