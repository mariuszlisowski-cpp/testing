#include <gtest/gtest.h>
#include <string>

/* returns the string without numbers and with spaces changed into ‘_’ */
#include "stringLib.hpp"

// struct StringLibTest : public ::testing::Test {
//     std::string input = "a1 b2 c3 d4";

// };

TEST(SpacesToUnderscoresRemoveNumbersTest, ShouldRemoveAllNumbers) {
    std::string input = "a1 b2 c3 d4";
    std::string expected("a_b_c_d");
    EXPECT_EQ(spacesToUnderscoresRemoveNumbers(input), expected);
}

TEST(SpacesToUnderscoresRemoveNumbersTest, ShouldReplaceAllSpacesWithUnderscore) {
    std::string input = "a b c d";
    std::string expected("a_b_c_d");
    EXPECT_EQ(spacesToUnderscoresRemoveNumbers(input), expected);
}

TEST(SameStringTest, StringsShouldBeTheSame) {
    EXPECT_TRUE(sameString("one", "one"));
}

TEST(SameStringTest, StringsShouldBeDifferent) {
    EXPECT_FALSE(sameString("one", "ONE"));
}

TEST(ErrorStringTest, ShouldRetunProperErrorCode) {
    EXPECT_STREQ(errorString(), "error 1");
    EXPECT_STRCASEEQ(errorString(), "Error 1");                 // case INSENSITIVE
}
