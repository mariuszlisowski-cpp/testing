#include <gtest/gtest.h>

#include "calendar_mock.hpp"
#include "days.hpp"
#include "drink_beer.hpp"

struct DrinkBeerTest : public ::testing::Test {
    CalendarMock calendar_mock;
    DrinkBeer uut{&calendar_mock};                                      // mocked calendar injection
};

TEST_F(DrinkBeerTest, CannotDrinkBeerOnWednesday) {
    /* arrange */
    EXPECT_CALL(calendar_mock, get_weekday)                             // mock returning
        .WillOnce(::testing::Return(Days::WEDNESDAY));                  // Wednesday once
    /* act */
    auto result = uut.can_drink_beer_today();                           // calls get_weekday()
    /* assert */
    EXPECT_FALSE(result);
}

TEST_F(DrinkBeerTest, CanDrinkBeerOnSaturday) {
    /* arrange */
    EXPECT_CALL(calendar_mock, get_weekday)
        .WillOnce(::testing::Return(Days::SATURDAY));
    /* act */
    /* assert */
    EXPECT_TRUE(uut.can_drink_beer_today());                            // calls get_weekday()
}

TEST_F(DrinkBeerTest, CanDrinkBeerOnSunday) {
    /* arrange */
    EXPECT_CALL(calendar_mock, get_weekday)
        .WillOnce(::testing::Return(Days::SUNDAY));
    /* act */
    /* assert */
    EXPECT_FALSE(uut.can_drink_beer_today());                            // calls get_weekday()
}
