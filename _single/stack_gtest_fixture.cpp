/* g++ stack_fixture.cpp -std=c++17 -lgtest -pthread */
#include <gtest/gtest.h>
#include <iostream>
#include <optional>
#include <vector>

class Stack {
public:
    explicit Stack(std::vector<int>&& stack) : stack_(stack) {}
    void push(int value) {
        stack_.push_back(value);
    }

    auto pop() {
        if (!stack_.size()) {
            return std::optional<int>();
        }
        auto back = stack_.back();
        stack_.pop_back();
     
        return std::optional<int>(back);
    }
    int top() const {
        return stack_.back();
    }
    size_t size() const {
        return stack_.size();
    }

private:
    std::vector<int> stack_;
};

struct StackTestFixtrure : public ::testing::Test {
    Stack* cut{};
    void SetUp() override {
        cut = new Stack({ 1, 2, 3, 4, 5 });
    }
    void TearDown() override {
        delete cut;
    }
};

TEST_F(StackTestFixtrure, ShouldReturnProperSize) {
    /* arrange - arranged in fixture */
    /* act -  no need to act */
    /* asset */
    ASSERT_EQ(cut->size(), 5);
}

TEST_F(StackTestFixtrure, ShouldReturnTopOfStack) {
    /* arrange - arranged in fixture */
    /* act -  no need to act */
    /* asset */
    ASSERT_EQ(cut->top(), 5);
}

TEST_F(StackTestFixtrure, ShouldPushNewValue) {
    /* arrange - arranged in fixture */
    /* act */
    cut->push(42);
    /* asset */
    ASSERT_EQ(cut->top(), 42);
}

TEST_F(StackTestFixtrure, ShouldPopFromTheTop) {
    /* arrange - arranged in fixture */
    /* act */
    cut->pop();
    /* asset */
    ASSERT_EQ(cut->top(), 4);
}

TEST_F(StackTestFixtrure, ShouldRetunPopedValue) {
    /* arrange - arranged in fixture */
    /* act */
    auto result = cut->pop();
    /* asset */
    ASSERT_TRUE(result.has_value());
    ASSERT_EQ(result.value(), 5);
}

TEST_F(StackTestFixtrure, ShouldPopAllValues) {
    /* arrange - arranged in fixture */
    /* act */
    while (cut->size()) {
        cut->pop();
    }
    /* asset */
    ASSERT_EQ(cut->size(), 0);
    // ASSERT_FALSE(result.has_value());
}

TEST_F(StackTestFixtrure, ShouldPopAllValuesAndReturnLastPopedValue) {
    /* arrange - arranged in fixture */
    /* act */
    std::optional<int> result;
    while (cut->size()) {
        result = cut->pop();
    }
    /* asset */
    ASSERT_EQ(result.value(), 1);
}

TEST_F(StackTestFixtrure, ShouldPopAllValuesAndReturnEmptyOptional) {
    /* arrange - arranged in fixture */
    /* act */
    while (cut->size()) {
        cut->pop();
    }
    auto result = cut->pop();
    /* asset */
    ASSERT_FALSE(result.has_value());
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
