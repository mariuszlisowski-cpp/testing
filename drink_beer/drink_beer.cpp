#include "drink_beer.hpp"
#include "calendar.hpp"

DrinkBeer::DrinkBeer(ICalendar* calendar) : calendar_(calendar) {}      // calendar injection

bool DrinkBeer::can_drink_beer_today() {
    const auto today = calendar_->get_weekday();                        // method cannot be tested
    return today == Days::FRIDAY ||                                     // thus must be mocked
           today == Days::SATURDAY;
}
