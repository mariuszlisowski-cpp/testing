#include "sort_descending.hpp"

#include <algorithm>
#include <functional>

void sort_descending(std::vector<int>& v) {
    std::sort(begin(v), end(v), std::greater());
}
