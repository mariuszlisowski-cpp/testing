#include "factorial_tgamma.hpp"

#include <cmath>

int factorial_tgamma(int n) {
    return n == 0 ? 1 : static_cast<int>(std::tgamma(n + 1));
}
