#include <gtest/gtest.h>

#include "tested_unit.hpp"
#include "legacy_class_mock.hpp"

using namespace ::testing;

TEST(example, test) {
    /* arrange */
    auto* legacy_class = new LegacyClassMock();             // will be deleted by...
    TestedUnit sut{legacy_class};                           // TestedUnit class
    EXPECT_CALL(*legacy_class, actionOne);                  // dereference to get an object
    /* act */
    sut.invokeActionOne();
    /* assert */
}
