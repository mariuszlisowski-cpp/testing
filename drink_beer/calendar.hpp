#pragma once

#include "icalendar.hpp"
#include "days.hpp"

/* production calendar */
class Calendar : public ICalendar {
public:
    Days get_weekday() override;                                    // must be replaced by mock for testing
};                                                                  // as it only returns current day
