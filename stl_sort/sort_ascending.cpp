#include "sort_ascending.hpp"

#include <algorithm>

void sort_ascending(std::vector<int>& v) {
    std::sort(begin(v), end(v));
}
