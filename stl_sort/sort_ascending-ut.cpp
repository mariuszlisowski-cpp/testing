#include <gtest/gtest.h>

#include "sort_ascending.hpp"

struct UnsortedVectorTest : public ::testing::Test {
    std::vector<int> v{ 2, 1, 4, 5, 3 };
};

TEST_F(UnsortedVectorTest, ShouldSortAscending) {
    auto sorted = std::vector<int>{ 1, 2, 3, 4, 5 };
    sort_ascending(v);
    EXPECT_EQ(v, sorted);
}
