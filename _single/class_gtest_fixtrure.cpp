/* g++ test_fixture.cpp -std=c++17 -lgtest -pthread */
#include <gtest/gtest.h>
#include <iostream>

class Clazz {
public:
    explicit Clazz(int value) : value_(value) {}

    void increment(int by_value) {
        value_ += by_value;
    }
    int get_value() const {
        return value_;
    }

private:
    int value_;
};

struct ClazzTestFixtrure : public ::testing::Test {
    Clazz* sut{};
    void SetUp() override {
        sut = new Clazz(-5);
    }
    void TearDown() override {
        delete sut;
    }
};


TEST_F(ClazzTestFixtrure, IncrementBy5) {
    /* arrange
    arranged in fixture */
    /* act */
    sut->increment(5);
    /* asset */
    ASSERT_EQ(sut->get_value(), 0);
}

TEST_F(ClazzTestFixtrure, IncrementBy10) {
    /* arrange
    arranged in fixture */
    /* act */
    sut->increment(10);
    /* asset */
    ASSERT_EQ(sut->get_value(), 5);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
