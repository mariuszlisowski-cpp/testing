#pragma once

#include <gmock/gmock.h>

#include "icalendar.hpp"
#include "days.hpp"

class CalendarMock : public ICalendar {
public:
    MOCK_METHOD(Days, get_weekday, (), (override));
};
