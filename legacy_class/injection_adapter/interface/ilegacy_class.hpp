#pragma once

class ILegacyClass {                                            // what if not trivial c'tor
public:                                                         // will be called with Mock
    virtual ~ILegacyClass() = default;                          // which derives from this class

    virtual void actionOne() = 0;                               // what if more methods are added
    virtual void actionTwo() = 0;                               // they should be also mocked
                                                                // to support this class for mocking

};                                                              // ANSWER: create interface as this
