#include <gtest/gtest.h>

#include "sort_descending.hpp"

struct UnsortedVectorTest : public ::testing::Test {
    std::vector<int> v{ 2, 1, 4, 5, 3 };
};

TEST_F(UnsortedVectorTest, ShouldSortDescending) {
    auto sorted = std::vector<int>{ 5, 4, 3, 2, 1 };
    sort_descending(v);
    EXPECT_EQ(v, sorted);
}
